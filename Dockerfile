From docker-release.pruregistry.intranet.asia:8443/ubuntu:18.04
RUN apt-get update -y
RUN apt-get install ca-certificates apt-transport-https git -y
COPY microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-bionic-prod bionic main" > /etc/apt/sources.list.d/dotnetdev.list
RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli bionic main" > /etc/apt/sources.list.d/azure-cli.list
RUN apt-get update -y
RUN apt-get install azure-functions-core-tools azure-cli -y
COPY Python-3.6.5.tgz /tmp
RUN tar -xvf /tmp/Python-3.6.5.tgz
RUN apt-get install make zlib1g-dev gcc -y
RUN cd Python-3.6.5 \
    && ./configure \
    && make \
    && make install \
    && rm -rf /tmp/Python-3.6.5.tgz
RUN apt-get clean -y
RUN mkdir ~/.ssh
COPY id_rsa /tmp
RUN mv /tmp/id_rsa /root/.ssh/ \
    && chmod 600 /root/.ssh/id_rsa
